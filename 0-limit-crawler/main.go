//////////////////////////////////////////////////////////////////////
//
// Your task is to change the code to limit the crawler to at most one
// page per second, while maintaining concurrency (in other words,
// Crawl() must be called concurrently)
//
// @hint: you can achieve this by adding 3 lines
//

package main

import (
	"fmt"
	"sync"
	"time"
)

func Crawl(url string, depth int, wg *sync.WaitGroup, timeLimit <-chan time.Time) {
	defer wg.Done()

	if depth <= 0 {
		return
	}

	body, urls, err := fetcher.Fetch(url)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Printf("found: %s %q\n", url, body)

	wg.Add(len(urls))

	for _, u := range urls {
		<-timeLimit //Ticking
		go Crawl(u, depth-1, wg, timeLimit)
	}
	return
}

func main() {
	var wg sync.WaitGroup
	timeLimit := time.Tick(time.Second) //Creating Ticker

	wg.Add(1)
	Crawl("http://golang.org/", 4, &wg, timeLimit)
	wg.Wait()
}
